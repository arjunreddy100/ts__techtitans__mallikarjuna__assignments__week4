package jdbcconnection;

import java.sql.*;

public class JDBCconnectionInsertUpdateDeleteSelect {

	public static void main(String[] args) {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/products", "root", "1234");
			PreparedStatement statement = connection.prepareStatement("insert into product values(?,?,?)");
			statement.setInt(1, 201);
			statement.setString(2, "Reddy");
			statement.setInt(3, 60000);
			int object = statement.executeUpdate();
			System.out.println(object + "  Rows updated.");
			ResultSet resultset = statement.executeQuery("select * from product");
			while (resultset.next()) {
				System.out.println(resultset.getInt(1) + " " + resultset.getString(2) + " " + resultset.getInt(3));
			}
			connection.close();
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

}
