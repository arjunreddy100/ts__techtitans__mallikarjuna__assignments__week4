package exceptionconcepts;

import java.util.*;

public class ExceptionHandling extends Exception {
	public void division() {
		int a = 10, b = 0;
		int c = a / b;
	}

	public static void main(String[] args) {
		ExceptionHandling obj = new ExceptionHandling();
		try {
			obj.division();
		} catch (Exception e) {
			System.out.println("warning exception!" + e);// e.printStackTrace();
		}

	}

}
