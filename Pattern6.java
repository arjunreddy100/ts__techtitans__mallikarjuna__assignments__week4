package numberpatterns;

public class Pattern6 {

	public static void main(String[] args) {
		for (int i = 1; i <= 5; i++) {
			for (int j = 5; j > 5 - i; j--) {
				System.out.print(j);
			}

			for (int j = 1; j <= 5 - i; j++) {
				System.out.print(5 - i + 1);
			}

			System.out.println();
		}

	}
}
/*
55555
54444
54333
54322
54321
             */
