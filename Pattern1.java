package numberpatterns;

public class Pattern1 {

	public static void main(String[] args) {
		for (int i = 1; i <= 5; i++) {
			if (i == 1 || i == 5) // for first row and 5th row
			{
				for (int j = 1; j <= 5; j++) {
					if (j == 1 || j == 5) // for first column and 5th column
					{
						System.out.print("0"); // print 0
					} else {
						System.out.print("1"); // otherwise print 1.
					}

				}
			} else {
				for (int j = 1; j <= 5; j++)
					if (j == 1 || j == 5) {
						System.out.print("1");
					} else {
						System.out.print("0");
					}
			}
			System.out.println();
		}
	}

}
/*
01110
10001
10001
10001
01110
        */