package jdbcconnection;

import java.sql.*;

public class JDBCconnectionREmoveColumn {

	public static void main(String[] args) {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/products", "root", "1234");
			PreparedStatement object = connection
					.prepareStatement("insert into product(pid,p_name,p_price,location) values(?,?,?,?)");
			// int the above statement products is the database name.
			object.setInt(1, 100);
			object.setString(2, "arjun");
			object.setFloat(3, 50000);
			object.setString(4, "chennai");
			int result = object.executeUpdate();
			System.out.println(result + " rows updated.");
			connection.close();
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

}
