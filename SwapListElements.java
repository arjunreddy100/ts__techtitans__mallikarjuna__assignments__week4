package collections.linkedlistexamples;

import java.util.*;

public class SwapListElements {

	public static void main(String[] args) {
		LinkedList link = new LinkedList(); // creating a linked list
		link.add("Hi"); // adding elements into list.
		link.add("wello");
		link.add("are");
		link.add("you");
		link.add(100);
		System.out.println("before swap :"+link);
		Collections.swap(link, 1, 2);
		System.out.println("after swap :"+link);	
		
	}

}
