package patterns;

public class pattern_2 {

	public static void main(String[] args) {
		for (int i = 1; i <= 5; i++) {
			int k = 6 - i; // to print cross we have to take another variable which is decreasing by i.
			for (int j = 1; j <= 5; j++) {

				if (i == 3 || j == 3 || i == j || j == k) { // for plus i==3 and j==3,for 'x' i==j and j==k
					if (i == 3 && j == 3) { // for middle space we created this if condition.
						System.out.print("");
					} else {
						System.out.print("* ");
					}
				} else {

					System.out.print(" ");
				}
			}
			System.out.println();

		}
	}

}
/*
*  *  * 
 * * *  
* * * * 
 * * *  
*  *  * 
              */