package comparator;

public class Student {
	int stu_id;
	String stu_name;
	int stu_age;

	public Student(int stu_id, String stu_name, int stu_age) {
		this.stu_id = stu_id;
		this.stu_name = stu_name;
		this.stu_age = stu_age;
	}

}
