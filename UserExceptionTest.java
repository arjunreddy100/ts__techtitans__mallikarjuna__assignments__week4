package exceptionconcepts;

import java.util.*;
import java.lang.*;

public class UserExceptionTest extends Exception {
	public void check(double bal) throws LowBalanceException {
		throw new LowBalanceException(bal);
	}

	public static void main(String[] args) {
		try {
			double bal;
			System.out.println("Enter balance to check:");
			Scanner scanner = new Scanner(System.in);
			bal = scanner.nextDouble();
			UserExceptionTest obj = new UserExceptionTest();
			obj.check(bal);

		} catch (Exception e)

		{
			System.out.println(e);
		}
	}

}
