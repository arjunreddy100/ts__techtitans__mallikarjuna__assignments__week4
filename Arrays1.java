package arraysconcept;

import java.util.*;

public class Arrays1 {
	static float array[] = new float[5];
	static float sum = 0;
	float avg = 0;

	public float addition(float array[]) {
		for (int j = 0; j < array.length; j++) {
			sum = sum + array[j];
		}
		avg = sum / array.length;
		// return sum;
		return avg; // returns average of all numbers.
	}

	public static void main(String[] args) {
		Arrays1 object = new Arrays1();
		Scanner scanner = new Scanner(System.in);
		System.out.println("enter values into array:");
		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextFloat();
		}
		// obj.addition(array);
		// System.out.println("the sum is:"+obj.addition(array));
		System.out.println("the average is:" + object.addition(array));
	}

}