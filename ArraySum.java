package arraysconcept;

/*
import java.util.Arrays;
import java.util.*;

public class Excercise12 {
	public static void main(String[] args) {
		int[] my_array = { 1, 2, 5, 5, 6, 6, 7, 2, 2, 1, 11, 1, 1, 1, 1, 1 };
		HashSet set = new HashSet();
		HashSet set1 = new HashSet();
		for (int i = 0; i < my_array.length - 1; i++) {
			for (int j = i + 1; j < my_array.length; j++) {
				if ((my_array[i] == my_array[j]) && (i != j)) {
					set.add(my_array[j]);

				} else {
					set1.add(my_array[j]);
				}
			}
		}
		System.out.println("Duplicate Elements are : " + set);
		System.out.println("Remaining Elements without duplicates are : " + set1);
	}
}*/
import java.util.*;

public class ArraySum {
	static int ar[] = new int[1000];
	static int sum, number;

	public int simpleArraySum(int[] ar) {
		for (int i = 0; i < ar.length; i++) {
			sum = sum + ar[i];
		}
		return sum;
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		number = sc.nextInt();
		for (int i = 0; i < number; i++) {
			ar[i] = sc.nextInt();
		}
		ArraySum object = new ArraySum();
		System.out.println(object.simpleArraySum(ar));
	}

}
