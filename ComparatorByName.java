package comparator;

import java.util.*;

public class ComparatorByName implements Comparator<Student> {
	public int compare(Student student1, Student student2) {
		/*
		 * Student student1=(Student)obj1; Student student2=(Student)obj2;
		 */
		return student1.stu_name.compareTo(student2.stu_name);// descending order
		// return student2.stu_name.compareTo(student1.stu_name); //interchange student1 and student2 we get
		// ascending order.
	}

}
