package collections.hashsetexamples;

import java.util.HashSet;
import java.util.Iterator;

public class IterateHashSet {

	public static void main(String[] args) {
		HashSet set=new HashSet<>();
		set.add(2);
		set.add(2);
		set.add("reddy");
		set.add("arjun");
		
		Iterator iter=set.iterator();  // creating iterator object and assigning set into that.
		while(iter.hasNext())
		{
		System.out.println(iter.next());
		}
	}

}
