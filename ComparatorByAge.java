package comparator;

import java.util.Comparator;

public class ComparatorByAge implements Comparator<Student> {
	public int compare(Student student1, Student student2) {
		if (student1.stu_age < student2.stu_age) {
			return 1;
		} else if (student1.stu_age > student2.stu_age) {
			return -1;
		} else {
			return 0;
		}
	}

}
