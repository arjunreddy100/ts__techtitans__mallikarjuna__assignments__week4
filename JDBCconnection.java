package jdbcconnection;

import java.sql.*;

public class JDBCconnection {

	public static void main(String[] args) {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/products", "root", "1234");
			// products is the database name and root is the local instance and password is
			// 1234.
			Statement stmt = connection.createStatement(); // the statement which is used for crate a query
			// ResultSet rs=stmt.executeQuery("select * from product");//the result from
			// executeQuery() method will store into result set object.
			int rs = stmt.executeUpdate("INSERT INTO product " + "VALUES (105, 'phone', 18000)");
			System.out.println(+rs + "  rows updated");
			/*
			 * while(rs.next()) //print by iteration by checking next column
			 * System.out.println(rs.getInt(1)+"  "+rs.getString(2)+"  "+rs.getString(3));
			 */ // printing the columns.
			
			connection.close(); // closing the connetion.
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
