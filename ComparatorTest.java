package comparator;

import java.util.*;

public class ComparatorTest {

	public static void main(String[] args) {
		List list_object = new ArrayList();

		list_object.add(new Student(8, "Malli", 21));
		list_object.add(new Student(3, "Arjun reddy", 22));
		list_object.add(new Student(5, "Kethi Arjun", 20));
		Collections.sort(list_object, new ComparatorByName());
		Iterator iter_obj = list_object.iterator();
		while (iter_obj.hasNext()) {
			Student stu1 = (Student) iter_obj.next();
			System.out.println(stu1.stu_id + "," + stu1.stu_name + "," + stu1.stu_age);
		}

	}

}
