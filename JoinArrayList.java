package collections.arraylistexamples;
import java.util.ArrayList;
import java.util.Collections;
public class JoinArrayList {

	public static void main(String[] args) {
		   ArrayList<String> list1= new ArrayList<String>(); //create a list
		          list1.add("Arjun");
		          list1.add("malli");        //add elements into list
		          list1.add("mahe");
		          list1.add("hello");
		          list1.add("and");
		          System.out.println("List of first array list: " + list1);
		          ArrayList<String> list2= new ArrayList<String>();   //create another list
		          list2.add("welcome");
		          list2.add("america");
		          list2.add("london");
		          list2.add("japan");
		          System.out.println("List of second array list: " + list2);
		      
		        ArrayList<String> new_list = new ArrayList<String>(); //create third list to join.
		        new_list.addAll(list1);  //add all elements into new list.
		        new_list.addAll(list2); //add all elements into new list.
		        System.out.println("New array list: " + new_list);  //print the new list.
		        

  


	}

}
