package stringconcept;

import java.util.*;

public class ReplaceAWord {

	public static void main(String[] args) {
		String string = new String();
		String str;
		System.out.println("Enter a string :");
		Scanner scanner = new Scanner(System.in);
		string = scanner.nextLine();

		if (string.contentEquals("java")) {
			str = string.replace("java", "j2ee");

			System.out.println("The string value is : " + str);
		}

	}
}
