package collections.arraylistexamples;

import java.util.ArrayList;

public class IncreaseCapacity {

	public static void main(String[] args) {
		ArrayList<String> list1= new ArrayList<String>();
        list1.add("Arjun");
        list1.add("malli");        //add elements into list
        list1.add("mahe");
        list1.add("hello");
        System.out.println("Original array list: " + list1);
        System.out.println("increase capacity of array list ");
        list1.ensureCapacity(7);   //increase capacity of array to size.
        list1.add(null);
        list1.add(null);
        list1.add(null);
        System.out.println(list1);
        
        }

}
