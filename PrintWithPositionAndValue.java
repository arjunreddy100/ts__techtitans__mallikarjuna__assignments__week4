package collections.linkedlistexamples;

import java.util.LinkedList;

public class PrintWithPositionAndValue {

	public static void main(String[] args) {
		LinkedList link = new LinkedList(); // creating a linked list
		link.add("Hi"); // adding elements into list.
		link.add("wello");
		link.add("are");
		link.add("you");
		link.add(100);
		
		for(int i=0;i<link.size();i++)
		{
			System.out.println("the value at position "+i+"is "+link.get(i));
		}
	}

}
