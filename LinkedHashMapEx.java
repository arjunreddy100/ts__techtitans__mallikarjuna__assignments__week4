package collections.MapConcepts;
import java.util.*;
public class LinkedHashMapEx {  //insertion order based and no duplicates.

	public static void main(String[] args) {
		LinkedHashMap<Integer,String> link=new LinkedHashMap<Integer,String>();
		link.put(1, "id00");
		link.put(5, "10000");
		link.put(2, "vau");
		link.put(3, "vali");
		for(Map.Entry obj:link.entrySet()) //to print iteration we use for loop.
		{
			System.out.println(obj.getKey()+"="+obj.getValue());
		}
		System.out.println(link);

	}

}
