package numberpatterns;

public class Pattern4 {

	public static void main(String[] args) {
		for (int i = 0; i <= 4; i++) {
			for (int j = 1; j <= 5; j++) {
				System.out.print(j + i);
			}
			System.out.println();
		}

	}

}
/*
12345
23456
34567
45678
56789
                 */
