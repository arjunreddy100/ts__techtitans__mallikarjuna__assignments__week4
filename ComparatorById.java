package comparator;

import java.util.Comparator;

public class ComparatorById implements Comparator {
	public int compare(Object obj1, Object obj2) {
		Student student1 = (Student) obj1;
		Student student2 = (Student) obj2;

		if (student1.stu_id < student2.stu_id) {
			return 1;
		} else if (student1.stu_id > student2.stu_id) {
			return -1;
		} else {
			return 0;
		}
	}

}
