package collections.linkedlistexamples;

import java.util.*;

public class InsertSpecificPosition {

	public static void main(String[] args) {
		LinkedList link = new LinkedList(); // creating a linked list
		link.add("Hi"); // adding elements into list.
		link.add("wello");
		link.add("are");
		link.add("you");
		link.add(100);
		System.out.println("Before inserting element :" + link);
		link.add(2, 20); // adding value into 2nd index position.
		System.out.println("After inserting element in 2nd position :" + link); // printing.
	}

}
