package multithreading;

import java.util.*;

public class GetThreadIdName implements Runnable {
	public void run() {
		System.out.println("The current thread id is :" + Thread.currentThread().getId());
		Thread.currentThread().setName("arjun");
		System.out.println("The current thread name is :" + Thread.currentThread().getName());

	}

	public static void main(String[] args) {

		GetThreadIdName object = new GetThreadIdName();
		Thread threadobject = new Thread(object);
		threadobject.start();
		System.out.println("The thread name is :" + Thread.currentThread().getName());
	}

}
