package collections.linkedlistexamples;

import java.util.*;
import java.util.LinkedList;

public class IterateLinkedList {

	public static void main(String[] args) {
		LinkedList link = new LinkedList(); // creating a linked list
		link.add("Hi"); // adding elements into list.
		link.add("wello");
		link.add("are");
		link.add("you");
		link.add(100);
		Iterator iter = link.listIterator(2); // listiterator() method to specify index to iterate from that position.
		while (iter.hasNext()) // check new line is there?
		{
			System.out.println(iter.next()); // print next word.
		}
	}

}
