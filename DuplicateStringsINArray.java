package arraysconcept;

import java.util.*;

public class DuplicateStringsINArray {

	public static void main(String[] args) {

		HashSet set = new HashSet();
		String string[] = new String[5];
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter strings into array :");

		for (int i = 0; i < string.length; i++) {
			string[i] = scanner.nextLine();
		}

		for (int i = 0; i < string.length; i++) {
			for (int j = i + 1; j < string.length; j++) {
				if (string[i].equals(string[j])) {
					set.add(string[j]);
				}
			}
		}
		System.out.println("The duplicate values are :" + set);

	}

}
