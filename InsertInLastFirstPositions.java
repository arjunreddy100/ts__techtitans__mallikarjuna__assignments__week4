package collections.linkedlistexamples;

import java.util.*;

public class InsertInLastFirstPositions {

	public static void main(String[] args) {
		LinkedList link = new LinkedList(); // creating a linked list
		link.add("Hi"); // adding elements into list.
		link.add("wello");
		link.add("are");
		link.add("you");
		link.add(100);
		System.out.println("Before inserting elements :" + link);
		link.addFirst(123);// adding value to first position to list.
		link.addLast(250); // adding value to last position to list.
		System.out.println("After inserting elements in first and last to list :" + link); // printing.

	}

}
