package collections.linkedlistexamples;
import java.sql.*;
import java.util.Collections;
import java.util.LinkedList;
public class ShuffleList {

	public static void main(String[] args) {
		LinkedList link = new LinkedList(); // creating a linked list
		link.add("Hi"); // adding elements into list.
		link.add("wello");
		link.add("are");
		link.add("you");
		link.add(100);
		System.out.println("before shuffle :"+link);  //Collections class.
		Collections.shuffle(link);  //to shuffle we use collections.shuffle() method.
		System.out.println("after shuffle :"+link);
	  link.set(1, 200);  //to replace 1st position with value 200.s
		
	}

}
