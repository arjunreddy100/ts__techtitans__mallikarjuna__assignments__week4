package collections.arraylistexamples;
import java.util.ArrayList;
import java.util.Collections;
public class TrimToSize {
	 
	  public static void main(String[] args) {
	          ArrayList<String> list1= new ArrayList<String>();
	          list1.add("Arjun");
	          list1.add("malli");        //add elements into list
	          list1.add("mahe");
	          list1.add("hello");
	          System.out.println("Original array list: " + list1);
	          System.out.println("To trim to size list array: ");
	          list1.trimToSize();     //trim array to size to save storage.
	          System.out.println(list1);
	   }
	}



