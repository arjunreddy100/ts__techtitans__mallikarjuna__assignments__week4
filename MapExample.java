package collections.MapConcepts;
import java.util.*;
public class MapExample {//order based no duplicates.

	public static void main(String[] args) {
		Map<Integer,String> map=new HashMap<Integer,String>();
map.put(6, "my name");
map.put(2, "arjun");
map.put(5, "hello");
map.put(0, "empty");
for (Map.Entry m:map.entrySet()) {
	System.out.println(m.getKey()+" "+m.getValue());
}
System.out.println(map);
	}

}
