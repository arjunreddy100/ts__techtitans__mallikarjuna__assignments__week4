package collections.linkedlistexamples;

import java.util.Iterator;
import java.util.LinkedList;

public class ReverseIterationList {

	public static void main(String[] args) {
		LinkedList link = new LinkedList(); // creating a linked list
		link.add("Hi"); // adding elements into list.
		link.add("wello");
		link.add("are");
		link.add("you");
		link.add(100);
		Iterator iter = link.descendingIterator(); // create iterable descending object.
		/*while (iter.hasNext()) // check new line is there?
		{
			System.out.println(iter.next()); // print in reverse order.
		}*/
		for(int i=0;i<link.size();i++)
		{
			System.out.println(link.get(i));
		}
	}

}
