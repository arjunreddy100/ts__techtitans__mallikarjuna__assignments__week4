package collections.hashsetexamples;

import java.util.HashSet;

public class EmptyAllElements {

	public static void main(String[] args) {
	
		HashSet set=new HashSet<>();  //creating new hash set.
		set.add(2);
		set.add(2);
		set.add("reddy");
		set.add("arjun");
		System.out.println("the elements are"+set);
		set.removeAll(set);     //to remove all elements from set.
		System.out.println("after removal:"+set);
	}

}
