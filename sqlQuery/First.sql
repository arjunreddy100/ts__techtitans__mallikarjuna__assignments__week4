use arjun;
create table student (sid int primary key,sname varchar(20) not null,dept varchar(20) not null default "engineering",section char not null);
drop table student;
select * from student;
desc student;
insert into student values(104,"Malli","Bsc",'A');
create table dept(did int not null,dname varchar(20) not null,sid int,foreign key(sid) references student(sid));
insert into dept values(2,"EEE",101);
select * from dept;
update dept set did=2 where dname="Bsc";
alter table dept  add column faculty varchar(20);
alter table dept modify faculty varchar(10);
update dept set faculty=2 where did=1;
desc dept;
select sid from dept group by did; 
select section from student group by section;
select sname from student order by sname desc;
select * from student where section in('c'); 
select * from student where sid between 100 and 104;
select * from student limit 3;
select * from student;
select sname from student where sname like "a%n";
use arjun;
delete from student  where section='a' ;
commit;
select * from dept;
update dept set faculty=null where did=1;
desc dept;
delete faculty from dept where did=1;

alter table dept add primary key (did) ;
create table faculty(fid int primary key auto_increment,fsalary float);
desc faculty;
alter table faculty add foreign key(fid) references dept(did);/*to create  */
drop table faculty;
insert into faculty values(1,10000);
insert into faculty(fsalary) values(20000);
select * from faculty;
insert into faculty(fsalary) values(40000);
use arjun;
select * from student;
select * from employee;
insert into employee values (1,"arjun","1996-06-15");
select date_format("1996-06-15","mm day year");
select dayname(curdate());
select sysdate();
select timestamp("1996-06-15");
select time_to_sec("00:02:00");
select datediff("1997-06-15","1996-06-15");
select timediff("3:2:3","2:2:3");
select user();
select floor("1,3.4,6");
select ceil('34,1,67,9');
select monthname(curdate());
select version();
select user();
select * from student;