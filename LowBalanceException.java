package exceptionconcepts;

public class LowBalanceException extends Exception {
	public LowBalanceException(double bal) {          //constructor to take the value.
		lowBalance(bal);            //method to execute.              
                                    
	}

	public void lowBalance(double bal) {
		if (bal <= 1000) {
			System.out.println("Low balance!Warning!");
		}
	}

}
