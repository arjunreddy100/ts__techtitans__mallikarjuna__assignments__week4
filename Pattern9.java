package numberpatterns;

public class Pattern9 {

	public static void main(String[] args) {
		for (int i = 1; i <= 5; i++) {
			for (int j = 0; j <= 4; j++) {
				System.out.print(j + 1);
			}
			System.out.println();
		}
	}

}
/*
12345
12345
12345
12345
12345
           */