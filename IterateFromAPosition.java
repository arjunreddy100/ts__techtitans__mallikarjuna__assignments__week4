package collections.linkedlistexamples;

import java.util.Iterator;
import java.util.LinkedList;

public class IterateFromAPosition {

	public static void main(String[] args) {
		LinkedList link = new LinkedList(); // creating a linked list
		link.add("Hi"); // adding elements into list.
		link.add("wello");
		link.add("are");
		link.add("you");
		link.add(100);
		Iterator iter = link.listIterator(); // create iterable object.
		while (iter.hasNext()) // check new line is there?
		{
			System.out.println(iter.next()); // print next word.
		}
	}
	}

