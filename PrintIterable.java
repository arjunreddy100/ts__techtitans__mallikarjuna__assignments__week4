package collections.arraylistexamples;
import java.util.Iterator;
import java.util.*;



public class PrintIterable {

	public static void main(String[] args) {
		ArrayList<String> list1= new ArrayList<String>();
        list1.add("Arjun");
        list1.add("malli");        //add elements into list
        list1.add("mahe");
        list1.add("hello");
        list1.add("how");
        list1.add("are");
        list1.add("you");
      Iterator<String> object=list1.iterator();
      System.out.println(list1);
      while(object.hasNext())
      {
    	  if(object.next()=="how")
    	  {
    		 continue; 
    	  }
    	 
    	  System.out.println(object.next());
      }

	}

}
