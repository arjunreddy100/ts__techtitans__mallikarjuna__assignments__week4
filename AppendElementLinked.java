package collections.linkedlistexamples;

import java.util.*;

public class AppendElementLinked {

	public static void main(String[] args) {

		int element;
		LinkedList link = new LinkedList();
		link.add("Hi"); // adding elements into list.
		link.add("wello");
		link.add("are");
		link.add("you");
		Scanner sc = new Scanner(System.in);
		System.out.println("enter an element :");
		element = sc.nextInt(); // taking integer input from user.
		System.out.println("before adding element list is :" + link);
		link.addLast(element); // adding the taken input to list.
		System.out.println("After adding element at last :" + link);

	}

}
