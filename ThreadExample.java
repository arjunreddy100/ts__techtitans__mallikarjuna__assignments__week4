package multithreading;

public class ThreadExample extends Thread {
	public void run() {
		System.out.println("Hello! How are you.");

	}

	public static void main(String[] args) {
		ThreadExample class_object = new ThreadExample();
		Thread object1 = new Thread(class_object);
		Thread object2 = new Thread(class_object);

		try {
			object1.start();
			object2.sleep(5000);

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		object2.start();

	}

}
