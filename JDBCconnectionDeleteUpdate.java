package jdbcconnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class JDBCconnectionDeleteUpdate {

	public static void main(String[] args) {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/products", "root", "1234");
			/*
			 * PreparedStatement stmt=
			 * con.prepareStatement("delete from product where pid=?"); stmt.setInt(1, 106);
			 * int obj=stmt.executeUpdate();
			 */

			// System.out.println(obj+" rows updated.");
			PreparedStatement stmt = con.prepareStatement("update product");
			int obj = stmt.executeUpdate("update product set p_name='samsung' where pid=102");
			System.out.println(obj + " rows updated.");
			/*
			 * ResultSet rs=stmt.executeQuery("Select * from product where pid=103 ");
			 * while(rs.next()) {
			 * System.out.println("the product names are :"+rs.getString(2)); }
			 */
			con.close();
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

}
